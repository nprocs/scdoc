#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "str.h"
#include "utf8.h"
#include "util.h"
#include "ver.h"

static struct str
parse_section(struct parser *p)
{
	uint32_t ch = 0;
	struct str section = {
		.str = (char *)xcalloc(16, sizeof(char)),
		.len = 0,
		.cap = 16,
	};

	while ((ch = xparser_get(p)) != ')') {
		if (ch < 0x80 && isalnum((unsigned char)ch)) {
			str_append(&section, ch);
			continue;
		}

		parser_fatal(p, "Expected alphanumerical character or )");
	}

	if (!section.len) {
		parser_fatal(p, "Expected manual section");
	}

	char *subsection = NULL;
	const long sec = strtol(section.str, &subsection, 10);

	if (section.str == subsection) {
		parser_fatal(p, "Expected section digit");
	}
	if (sec < 0 || sec > 9) {
		parser_fatal(p, "Expected section in [0, 9]");
	}

	return section;
}

static struct str
parse_extra(struct parser *p)
{
	struct str extra = {
		.str = (char *)xcalloc(16, sizeof(char)),
		.len = 0,
		.cap = 16,
	};
	extra.str[extra.len++] = '"';
	for (;;) {
		const uint32_t ch = xparser_get(p);
		if (ch == '\n') {
			parser_fatal(p, "Unclosed extra preamble field");
		}

		str_append(&extra, ch);
		if (ch == '"') {
			return extra;
		}
	}
}

static void
parse_preamble(struct parser *p)
{
	struct str extras[2] = {{.str = NULL}, {.str = NULL}};
	struct str section = {.str = NULL};
	char date[32] = {0};
	time_t date_time = 0;
	unsigned int ex = 0;
	const char *source_date_epoch = getenv("SOURCE_DATE_EPOCH");
	if (source_date_epoch) {
		char *endptr = NULL;
		errno = 0;
		const long long epoch = strtoll(source_date_epoch, &endptr, 10);
		if (errno) {
			perror("SOURCE_DATE_EPOCH: strtoll");
			exit(EXIT_FAILURE);
		}
		if (endptr == source_date_epoch) {
			fputs("SOURCE_DATE_EPOCH: No digits found\n", stderr);
			exit(EXIT_FAILURE);
		}
		if (*endptr != '\0') {
			fprintf(stderr,
				"SOURCE_DATE_EPOCH: Trailing garbage: %s\n",
				endptr);
			exit(EXIT_FAILURE);
		}
		if (epoch < 0) {
			fputs("SOURCE_DATE_EPOCH must be nonnegative\n",
				stderr);
			exit(EXIT_FAILURE);
		}
		date_time = (time_t)epoch;
	} else {
		date_time = time(NULL);
	}
	const struct tm *date_tm = gmtime(&date_time);
	if (!date_tm) {
		perror("gmtime");
		exit(EXIT_FAILURE);
	}

#ifdef __MINGW32__
	strftime(date, sizeof(date), "%Y-%m-%d", date_tm);
#else
	strftime(date, sizeof(date), "%F", date_tm);
#endif

	struct str name = {
		.str = (char *)xcalloc(16, sizeof(char)),
		.len = 0,
		.cap = 16,
	};
	for (;;) {
		const uint32_t ch = xparser_get(p);
		if (ch < 0x80 && isalnum((unsigned char)ch)) {
			str_append(&name, ch);
			continue;
		}
		switch (ch) {
		case '_':
		case '-':
		case '.':
			str_append(&name, ch);
			break;
		case '(':
			section = parse_section(p);
			break;
		case '"':
			if (ex == COUNTOF(extras)) {
				parser_fatal(p,
					"Too many extra preamble fields");
			}
			extras[ex++] = parse_extra(p);
			break;
		case '\n':
			if (!name.len) {
				parser_fatal(p, "Expected preamble");
			}
			if (!section.str) {
				parser_fatal(p, "Expected manual section");
			}
			fprintf(stdout, ".TH \"%s\" \"%s\" \"%s\"", name.str,
				section.str, date);

			for (unsigned int i = 0; i < COUNTOF(extras); i++) {
				// Strings are already double-quoted.
				if (extras[i].str) {
					fprintf(stdout, " %s", extras[i].str);
				}
			}
			putchar('\n');

			free(name.str);
			free(section.str);
			for (unsigned int i = 0; i < COUNTOF(extras); i++) {
				free(extras[i].str);
			}
			return;
		default:
			if (!section.str) {
				parser_fatal(p,
					"Name characters must be "
					"A-Z, a-z, 0-9, `-`, `_`, or `.`");
			}
			break;
		}
	}
}

enum formatting {
	FORMAT_BOLD = 1,
	FORMAT_UNDERLINE = 2,
};

static void
parse_format(struct parser *p, enum formatting fmt)
{
	if (p->flags) {
		if (p->flags != fmt) {
			char err[80] = {0};
			snprintf(err, sizeof(err),
				"Cannot nest inline formatting "
				"(began with %c at %u:%u)",
				p->flags == FORMAT_BOLD ? '*' : '_',
				p->fmt_line, p->fmt_col);
			parser_fatal(p, err);
		}
		fputs("\\fR", stdout);
	} else {
		switch (fmt) {
		case FORMAT_BOLD:
			fputs("\\fB", stdout);
			break;
		case FORMAT_UNDERLINE:
			fputs("\\fI", stdout);
			break;
		default:
			break;
		}
		p->fmt_line = p->line;
		p->fmt_col = p->col;
	}
	p->flags ^= fmt;
}

static bool
parse_linebreak(struct parser *p)
{
	uint32_t ch = xparser_get(p);
	if (ch != '+') {
		putchar('+');
		parser_push(p, ch);
		return false;
	}
	ch = xparser_get(p);
	if (ch != '\n') {
		putchar('+');
		parser_push(p, ch);
		parser_push(p, '+');
		return false;
	}
	ch = xparser_get(p);
	if (ch == '\n') {
		parser_fatal(p,
			"Explicit line breaks cannot be "
			"followed by a blank line");
	}
	parser_push(p, ch);
	fputs("\n.br\n", stdout);
	return true;
}

static void
parse_text(struct parser *p, bool reading_cstring)
{
	uint32_t next = 0;
	uint32_t last = ' ';
	bool chomp_next_indent = false;
	for (unsigned int i = 0;; i++) {
		uint32_t ch = xparser_get(p);

		// skip indentation if last was a linebreak
		// and we need to chomp indentation
		if (chomp_next_indent) {
			if (ch == '\t' || ch == ' ') {
				continue;
			}
			chomp_next_indent = false;
		}

		switch (ch) {
		case '\\':
			ch = xparser_get(p);
			switch (ch) {
			case '\\':
				fputs("\\e", stdout);
				break;
			case '`':
				fputs("\\`", stdout);
				break;
			case '\0':
				if (reading_cstring) {
					parser_fatal(p,
						"Unexpected end of line");
				}
				break;
			default:
				utf8_put(ch);
				break;
			}
			break;
		case '*':
			parse_format(p, FORMAT_BOLD);
			break;
		case '_':
			next = xparser_get(p);
			if (!isalnum((unsigned char)last)
				|| ((p->flags & FORMAT_UNDERLINE)
					&& !isalnum((unsigned char)next))) {
				parse_format(p, FORMAT_UNDERLINE);
			} else {
				utf8_put(ch);
			}

			if (reading_cstring && next == '\0') {
				return;
			}

			parser_push(p, next);
			break;
		case '+':
			if (parse_linebreak(p)) {
				last = '\n';
				chomp_next_indent = true;
			}
			break;
		case '\n':
			utf8_put(ch);
			return;
		case '.':
			if (!i) {
				// Escape . if it's the first character
				fputs("\\&.\\&", stdout);
				break;
			}
			// FALLTHROUGH
		case '\'':
			if (!i) {
				// Escape ' if it's the first character
				fputs("\\&'\\&", stdout);
				break;
			}
			// FALLTHROUGH
		case '!':
		case '?':
			last = ch;
			utf8_put(ch);
			// Suppress sentence spacing
			fputs("\\&", stdout);
			break;
		case '~':
			// Escape ~ to not render it with U+02DC
			fputs("\\(ti", stdout);
			break;
		case '^':
			// Escape ^ to not render it with U+02C6
			fputs("\\(ha", stdout);
			break;
		case '\0':
			if (reading_cstring) {
				return;
			}
			break;
		default:
			last = ch;
			utf8_put(ch);
			break;
		}
	}
}

static void
parse_heading(struct parser *p)
{
	unsigned int level = 0;
	uint32_t ch = 0;
	while ((ch = xparser_get(p)) != ' ') {
		if (ch != '#') {
			parser_fatal(p,
				"Invalid start of heading "
				"(probably needs a space)");
		}

		level++;
	}
	switch (level) {
	case 0:
		fputs(".SH ", stdout);
		break;
	case 1:
		fputs(".SS ", stdout);
		break;
	default:
		parser_fatal(p,
			"Only headings up to two levels deep are permitted");
	}
	for (;;) {
		ch = xparser_get(p);
		utf8_put(ch);
		if (ch == '\n') {
			break;
		}
	}
}

#define ROFF_MACRO(cmd) fputs("." cmd "\n", stdout)

static unsigned int
parse_indent(struct parser *p, unsigned int indent, bool write)
{
	unsigned int i = 0;
	uint32_t ch = 0;
	while ((ch = xparser_get(p)) == '\t') {
		i++;
	}
	parser_push(p, ch);
	if (ch == '\n' && indent) {
		// Don't change indent when we encounter empty lines.
		return indent;
	}
	if (write) {
		if (i > indent + 1) {
			parser_fatal(p, "Indented by an amount greater than 1");
		}
		if (i == indent + 1) {
			ROFF_MACRO("RS 4");
			return i;
		}
		while (i < indent) {
			ROFF_MACRO("RE");
			indent--;
		}
	}
	return i;
}

static int
list_header(int num)
{
	if (num == -1) {
		fputs(".IP \\(bu 4\n", stdout);
		return num;
	}
	fprintf(stdout, ".IP %d. 4\n", num);
	return num + 1;
}

static unsigned int
parse_list(struct parser *p, unsigned int indent, int num)
{
	if (xparser_get(p) != ' ') {
		parser_fatal(p, "Expected space before start of list entry");
	}
	ROFF_MACRO("PD 0");
	num = list_header(num);
	parse_text(p, false);
	for (;;) {
		indent = parse_indent(p, indent, true);
		uint32_t ch = xparser_get(p);
		switch (ch) {
		case ' ':
			if (xparser_get(p) != ' ') {
				parser_fatal(p,
					"Expected two spaces for "
					"list entry continuation");
			}
			parse_text(p, false);
			break;
		case '-':
		case '.':
			if (xparser_get(p) != ' ') {
				parser_fatal(p,
					"Expected space before "
					"start of list entry");
			}
			num = list_header(num);
			parse_text(p, false);
			break;
		default:
			ROFF_MACRO("PD");
			parser_push(p, ch);
			return indent;
		}
	}
}

static void
parse_literal(struct parser *p, unsigned int base_indent)
{
	if (xparser_get(p) != '`' || xparser_get(p) != '`'
		|| xparser_get(p) != '\n') {
		parser_fatal(p,
			"Expected ``` and a newline to begin literal block");
	}
	ROFF_MACRO("nf");
	ROFF_MACRO("RS 4");
	unsigned int stops = 0;
	bool check_indent = true;
	for (;;) {
		if (check_indent) {
			unsigned int indent =
				parse_indent(p, base_indent, false);
			if (indent < base_indent) {
				parser_fatal(p,
					"Cannot deindent in literal block");
			}
			while (indent > base_indent) {
				putchar('\t');
				indent--;
			}

			check_indent = false;
		}
		uint32_t ch = xparser_get(p);
		if (ch == '`') {
			if (++stops == 3) {
				if (xparser_get(p) != '\n') {
					parser_fatal(p,
						"Expected literal block "
						"to end with newline");
				}
				ROFF_MACRO("fi");
				ROFF_MACRO("RE");
				return;
			}
			continue;
		}

		while (stops) {
			putchar('`');
			stops--;
		}
		switch (ch) {
		case '.':
			fputs("\\&.", stdout);
			break;
		case '\'':
			fputs("\\&'", stdout);
			break;
		case '\\':
			ch = xparser_get(p);
			if (ch == '\\') {
				fputs("\\\\", stdout);
			} else {
				utf8_put(ch);
			}
			break;
		case '\n':
			check_indent = true;
			// FALLTHROUGH
		default:
			utf8_put(ch);
			break;
		}
	}
}

enum table_align {
	ALIGN_LEFT,
	ALIGN_CENTER,
	ALIGN_RIGHT,
	ALIGN_LEFT_EXPAND,
	ALIGN_CENTER_EXPAND,
	ALIGN_RIGHT_EXPAND,
};

struct table_row {
	struct table_cell *cell;
	struct table_row *next;
};

struct table_cell {
	struct table_cell *next;
	struct str contents;
	enum table_align align;
};

static void
parse_table(struct parser *p, uint32_t style)
{
	const struct table_cell *pcell = NULL;
	struct table_cell *curcell = NULL;
	struct table_cell *prev = NULL;
	struct table_row *currow = NULL;
	struct table_row *prevrow = NULL;
	struct table_row *table = NULL;
	int column = 0;
	int numcolumns = -1;
	uint32_t ch = 0;
	parser_push(p, '|');
	while ((ch = xparser_get(p)) != '\n') {
		switch (ch) {
		case '|':
			prevrow = currow;
			currow =
				(struct table_row *)xcalloc(1, sizeof(*currow));
			if (prevrow) {
				if (column != numcolumns && numcolumns != -1) {
					parser_fatal(p,
						"Each row must have the "
						"same number of columns");
				}
				numcolumns = column;
				column = 0;
				prevrow->next = currow;
			}
			curcell = (struct table_cell *)xcalloc(1,
				sizeof(*curcell));
			currow->cell = curcell;
			if (!table) {
				table = currow;
			}
			break;
		case ':':
			if (!currow) {
				parser_fatal(p,
					"Cannot start a column without "
					"starting a row first");
			}
			prev = curcell;
			curcell = (struct table_cell *)xcalloc(1,
				sizeof(*curcell));
			if (prev) {
				prev->next = curcell;
			}
			column++;
			break;
		case ' ':
			break;
		default:
			parser_fatal(p, "Expected either '|' or ':'");
		}
		if (ch != ' ') {
			ch = xparser_get(p);
			switch (ch) {
			case '[':
				curcell->align = ALIGN_LEFT;
				break;
			case '-':
				curcell->align = ALIGN_CENTER;
				break;
			case ']':
				curcell->align = ALIGN_RIGHT;
				break;
			case '<':
				curcell->align = ALIGN_LEFT_EXPAND;
				break;
			case '=':
				curcell->align = ALIGN_CENTER_EXPAND;
				break;
			case '>':
				curcell->align = ALIGN_RIGHT_EXPAND;
				break;
			case ' ':
				if (!prevrow) {
					parser_fatal(p,
						"No previous row to "
						"infer alignment from");
				}
				pcell = prevrow->cell;
				for (int i = 0; i <= column && pcell;
					i++, pcell = pcell->next) {
					if (i == column) {
						curcell->align = pcell->align;
						break;
					}
				}
				break;
			default:
				parser_fatal(p,
					"Expected one of "
					"'[', '-', ']', or ' '");
			}
			curcell->contents.str = (char *)xcalloc(16, 1);
			curcell->contents.len = 0;
			curcell->contents.cap = 16;
		}

		assert(curcell);

		// Continue cell
		switch (xparser_get(p)) {
		case ' ':
			// Read out remainder of the text
			while ((ch = xparser_get(p)) != '\n') {
				str_append(&curcell->contents, ch);
			}
			break;
		case '\n':
			break;
		default:
			parser_fatal(p, "Expected ' ' or a newline");
		}
		// Commit cell
		if (strstr(curcell->contents.str, "T{")
			|| strstr(curcell->contents.str, "T}")) {
			parser_fatal(p,
				"Cells cannot contain T{ or T} "
				"due to roff limitations");
		}
	}
	// Commit table
	ROFF_MACRO("TS");

	switch (style) {
	case '[':
		fputs("allbox;", stdout);
		break;
	case ']':
		fputs("box;", stdout);
		break;
	default:
		break;
	}

	// Print alignments first
	currow = table;
	while (currow) {
		curcell = currow->cell;
		while (curcell) {
			switch (curcell->align) {
			case ALIGN_LEFT:
				putchar('l');
				break;
			case ALIGN_CENTER:
				putchar('c');
				break;
			case ALIGN_RIGHT:
				putchar('r');
				break;
			case ALIGN_LEFT_EXPAND:
				fputs("lx", stdout);
				break;
			case ALIGN_CENTER_EXPAND:
				fputs("cx", stdout);
				break;
			case ALIGN_RIGHT_EXPAND:
				fputs("rx", stdout);
				break;
			default:
				break;
			}

			if (curcell->next) {
				putchar(' ');
			}

			curcell = curcell->next;
		}
		if (!currow->next) {
			putchar('.');
		}

		putchar('\n');
		currow = currow->next;
	}

	// Then contents
	currow = table;
	while (currow) {
		curcell = currow->cell;
		fputs("T{\n", stdout);
		while (curcell) {
			parser_push_str(p, curcell->contents.str);
			parse_text(p, true);
			if (curcell->next) {
				fputs("\nT}\tT{\n", stdout);
			} else {
				fputs("\nT}", stdout);
			}
			prev = curcell;
			curcell = curcell->next;
			free(prev->contents.str);
			free(prev);
		}
		putchar('\n');
		prevrow = currow;
		currow = currow->next;
		free(prevrow);
	}

	ROFF_MACRO("TE");
	fputs(".sp 1\n", stdout);
}

NORETURN static void
parse_document(struct parser *p)
{
	unsigned int indent = 0;
	for (;;) {
		indent = parse_indent(p, indent, true);
		uint32_t ch = xparser_get(p);
		switch (ch) {
		case ';':
			if (xparser_get(p) != ' ') {
				parser_fatal(p,
					"Expected space after ; "
					"to begin comment");
			}
			while (xparser_get(p) != '\n') {
			}
			break;
		case '#':
			if (indent) {
				parser_push(p, ch);
				parse_text(p, false);
				break;
			}
			parse_heading(p);
			break;
		case '-':
			indent = parse_list(p, indent, -1);
			break;
		case '.':
			ch = xparser_get(p);
			if (ch == ' ') {
				parser_push(p, ch);
				indent = parse_list(p, indent, 1);
			} else {
				parser_push(p, ch);
				parse_text(p, false);
			}
			break;
		case '`':
			parse_literal(p, indent);
			break;
		case '[':
		case '|':
		case ']':
			if (indent) {
				parser_fatal(p, "Tables cannot be indented");
			}
			parse_table(p, ch);
			break;
		case ' ':
			parser_fatal(p, "Tabs are required for indentation");
		case '\n':
			if (p->flags) {
				char err[80] = {0};
				snprintf(err, sizeof(err),
					"Expected %c before starting new line "
					"(began with %c at %u:%u)",
					p->flags == FORMAT_BOLD ? '*' : '_',
					p->flags == FORMAT_BOLD ? '*' : '_',
					p->fmt_line, p->fmt_col);
				parser_fatal(p, err);
			}
			ROFF_MACRO("PP");
			break;
		default:
			parser_push(p, ch);
			parse_text(p, false);
			break;
		}
	}
}

static void
output_scdoc_preamble(void)
{
	fputs(".\\\" Generated by scdoc " VERSION "\n"
		".\\\" Complete documentation for this program "
		"is not available as a GNU info page\n",
		stdout);
	// Fix weird quotation marks
	// https://bugs.debian.org/507673
	// https://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
	fputs(".ie \\n(.g .ds Aq \\(aq\n.el       .ds Aq '\n", stdout);
	// Disable hyphenation:
	ROFF_MACRO("nh");
	// Disable justification:
	ROFF_MACRO("ad l");
	fputs(".\\\" Begin generated content:\n", stdout);
}

int
main(int argc, char *argv[])
{
	if (argc == 2 && strcmp(argv[1], "-v") == 0) {
		fputs("scdoc " VERSION "\n", stdout);
		return EXIT_SUCCESS;
	}
	if (argc > 1) {
		fputs("Usage: scdoc < input.scd > output.roff\n", stderr);
		return EXIT_FAILURE;
	}
	output_scdoc_preamble();
	struct parser p = {.line = 1, .col = 1};
	parse_preamble(&p);
	parse_document(&p);
	return EXIT_SUCCESS;
}
