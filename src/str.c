#include "str.h"
#include "utf8.h"
#include "util.h"

static void
ensure_capacity(struct str *str, size_t len)
{
	if (len >= str->cap) {
		str->str = (char *)xrealloc(str->str, str->cap * 2);
		str->cap *= 2;
	}
}

void
str_append(struct str *str, uint32_t ch)
{
	ensure_capacity(str, str->len + utf8_len(ch));
	const size_t len = utf8_encode(&str->str[str->len], ch);
	str->len += len;
	str->str[str->len] = '\0';
}
