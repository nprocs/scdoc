#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "utf8.h"
#include "util.h"

NORETURN void
parser_fatal(const struct parser *parser, const char *err)
{
	fprintf(stderr, "Error at %u:%u: %s\n", parser->line, parser->col, err);
	exit(EXIT_FAILURE);
}

uint32_t
xparser_get(struct parser *parser)
{
	if (parser->qhead) {
		return parser->queue[--parser->qhead];
	}
	if (parser->str) {
		uint32_t cp = 0;
		const size_t len = utf8_decode(&cp, parser->str);

		if (!len) {
			parser_fatal(parser, "Invalid UTF-8");
		}

		if (cp == '\0') {
			parser->str = NULL;
			return cp;
		}

		parser->str += len;
		return cp;
	}

	const uint32_t ch = utf8_get();
	switch (ch) {
	case UTF8_INVALID:
		parser_fatal(parser, "Invalid UTF-8");
	case '\n':
		parser->line++;
		parser->col = 0;
		break;
	default:
		parser->col++;
		break;
	}

	return ch;
}

void
parser_push(struct parser *parser, uint32_t ch)
{
	assert(parser->qhead < COUNTOF(parser->queue));
	parser->queue[parser->qhead++] = ch;
}

void
parser_push_str(struct parser *parser, const char *str)
{
	parser->str = str;
}

void *
xcalloc(size_t nmemb, size_t size)
{
	void *ret = calloc(nmemb, size);
	if (!ret) {
		abort();
	}

	return ret;
}

void *
xrealloc(void *ptr, size_t size)
{
	void *ret = realloc(ptr, size);
	if (!ret) {
		abort();
	}

	return ret;
}
