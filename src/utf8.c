#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "utf8.h"

static size_t
utf8_size(uint8_t c)
{
	if (c <= 0x7F) {
		return 1;
	}

	if (c >= 0xC2 && c <= 0xDF) {
		return 2;
	}

	if (c >= 0xE0 && c <= 0xEF) {
		return 3;
	}

	if (c >= 0xF0 && c <= 0xF4) {
		return 4;
	}

	return 0;
}

static bool
is_surrogate(uint32_t ch)
{
	return ch >= 0xD800 && ch <= 0xDFFF;
}

static bool
is_out_of_range(uint32_t ch)
{
	return ch > 0x10FFFF;
}

size_t
utf8_len(uint32_t ch)
{
	if (ch <= 0x7F) {
		return 1;
	}

	if (ch <= 0x7FF) {
		return 2;
	}

	if (is_surrogate(ch) || is_out_of_range(ch)) {
		return 0;
	}

	if (ch <= 0xFFFF) {
		return 3;
	}

	return 4;
}

size_t
utf8_decode(uint32_t *cp, const char *src)
{
	static const uint8_t masks[] = {0x0, 0x7F, 0x1F, 0xF, 0x7};
	static const uint32_t overlong[] = {0x0, 0x0, 0x80, 0x800, 0x10000};

	const uint8_t *s = (const uint8_t *)src;
	const size_t size = utf8_size(s[0]);

	if (size == 1) {
		*cp = s[0];
		return size;
	}

	uint32_t ch = s[0] & masks[size];

	for (size_t i = 1; i < size; i++) {
		if ((s[i] & 0xC0) != 0x80) {
			*cp = 0;
			return 0;
		}

		ch <<= 6;
		ch |= s[i] & 0x3F;
	}

	if (is_surrogate(ch) || is_out_of_range(ch) || ch < overlong[size]) {
		*cp = 0;
		return 0;
	}

	*cp = ch;
	return size;
}

size_t
utf8_encode(char *dst, uint32_t ch)
{
	const size_t len = utf8_len(ch);

	switch (len) {
	case 0:
		break;
	case 1:
		dst[0] = (char)ch;
		break;
	case 2:
		dst[0] = (char)(0xC0 | ch >> 6);
		dst[1] = (char)(0x80 | (ch & 0x3F));
		break;
	case 3:
		dst[0] = (char)(0xE0 | ch >> 12);
		dst[1] = (char)(0x80 | (ch >> 6 & 0x3F));
		dst[2] = (char)(0x80 | (ch & 0x3F));
		break;
	default:
		dst[0] = (char)(0xF0 | ch >> 18);
		dst[1] = (char)(0x80 | (ch >> 12 & 0x3F));
		dst[2] = (char)(0x80 | (ch >> 6 & 0x3F));
		dst[3] = (char)(0x80 | (ch & 0x3F));
		break;
	}

	return len;
}

uint32_t
utf8_get(void)
{
	char buffer[UTF8_MAX_SIZE] = {0};
	uint32_t cp = 0;

	const int ch = getchar();
	if (ch == EOF) {
		exit(ferror(stdin) ? EXIT_FAILURE : EXIT_SUCCESS);
	}

	buffer[0] = (char)ch;
	const size_t size = utf8_size((uint8_t)ch);

	switch (size) {
	case 0:
		return UTF8_INVALID;
	case 1:
		return (uint32_t)ch;
	default:
		if (fread(&buffer[1], 1, size - 1, stdin) != size - 1) {
			exit(ferror(stdin) ? EXIT_FAILURE : EXIT_SUCCESS);
		}

		utf8_decode(&cp, buffer);
		return cp;
	}
}

void
utf8_put(uint32_t ch)
{
	char buffer[UTF8_MAX_SIZE] = {0};
	const size_t size = utf8_encode(buffer, ch);
	fwrite(buffer, 1, size, stdout);
}
