# Fork notes

This is my personal fork of Drew DeVault's
[scdoc](https://git.sr.ht/~sircmpwn/scdoc).

No functional changes have been made, aside from stricter UTF-8 decoding.

The following is a non-exhaustive list of changes.

## Changes

- Add `const` type qualifier to pointers where applicable;

- Add `noreturn` to the signature of `parser_fatal`.

- Add explicit type conversions to silence `GCC`'s `-Wconversion` warnings;

- Cleanup `#include` directives (remove unnecessary headers);

- Fix a memory leak in `parse_preamble`.

- Format the code according to Drew DeVault's
  [C style guide](https://git.sr.ht/~sircmpwn/cstyle);

- Initialize all variables;

- Merge all `utf8` files;

- Migrate to C11 for the `_Noreturn` function specifier;

- POSIXify the `Makefile` (in conformance with the POSIX.1-2024 standard);

- Refactor `test` files to use POSIX `grep` options;

- Remove unused functions;

- Rename `string.c`/`string.h` to `str.c`/`str.h`;

- Replace the `roff_macro` function with the `ROFF_MACRO` macro
  (the function was always called with two arguments);

- Set return type of certain functions to `void`
  (those that didn't provide useful information to the caller);

- Silence several warnings from `GCC`, `cppcheck`, and `clang-lint`;

- Support C++ compilation;

- Support `MinGW` (fix a compilation error in `parse_preamble`).
