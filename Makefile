.POSIX:
.SILENT: check
.SUFFIXES:

CC = cc
CFLAGS = -std=c11 -g -O2 -Wall -Wextra -Werror -Wpedantic

PREFIX = /usr/local
BINDIR = $(PREFIX)/bin
MANDIR = $(PREFIX)/share/man

OBJ = main.o str.o utf8.o util.o

scdoc: $(OBJ)
	$(CC) $(LDFLAGS) -o $@ $(OBJ)

main.o: src/main.c
	$(CC) -c $(CFLAGS) -Iinclude -o $@ $?

str.o: src/str.c
	$(CC) -c $(CFLAGS) -Iinclude -o $@ $?

utf8.o: src/utf8.c
	$(CC) -c $(CFLAGS) -Iinclude -o $@ $?

util.o: src/util.c
	$(CC) -c $(CFLAGS) -Iinclude -o $@ $?

scdoc.1: scdoc.1.scd scdoc
	./scdoc < scdoc.1.scd > $@

scdoc.5: scdoc.5.scd scdoc
	./scdoc < scdoc.5.scd > $@

check: scdoc
	find test -perm -111 -exec '{}' \;

clean:
	rm -f $(OBJ) scdoc scdoc.1 scdoc.5

install: scdoc scdoc.1 scdoc.5
	mkdir -p \
		$(DESTDIR)$(BINDIR) \
		$(DESTDIR)$(MANDIR)/man1 \
		$(DESTDIR)$(MANDIR)/man5
	install scdoc $(DESTDIR)$(BINDIR)/scdoc
	install -m644 scdoc.1 $(DESTDIR)$(MANDIR)/man1/scdoc.1
	install -m644 scdoc.5 $(DESTDIR)$(MANDIR)/man5/scdoc.5

uninstall:
	rm -f \
		$(DESTDIR)$(BINDIR)/scdoc \
		$(DESTDIR)$(MANDIR)/man1/scdoc.1 \
		$(DESTDIR)$(MANDIR)/man5/scdoc.5

.PHONY: check clean install uninstall
