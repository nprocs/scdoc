#ifndef SCDOC_PARSER_H
#define SCDOC_PARSER_H
#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
#define NORETURN [[noreturn]]
#else
#define NORETURN _Noreturn
#endif

#define COUNTOF(arr) (sizeof(arr) / sizeof((arr)[0]))

struct parser {
	const char *str;
	unsigned int line, col;
	unsigned int flags;
	unsigned int fmt_line, fmt_col;
	unsigned int qhead;
	uint32_t queue[8];
};

NORETURN void parser_fatal(const struct parser *parser, const char *err);
uint32_t xparser_get(struct parser *parser);
void parser_push(struct parser *parser, uint32_t ch);
void parser_push_str(struct parser *parser, const char *str);
void *xcalloc(size_t nmemb, size_t size);
void *xrealloc(void *ptr, size_t size);

#endif
