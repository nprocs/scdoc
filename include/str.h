#ifndef SCDOC_STR_H
#define SCDOC_STR_H
#include <stddef.h>
#include <stdint.h>

struct str {
	char *str;
	size_t len;
	size_t cap;
};

void str_append(struct str *str, uint32_t ch);

#endif
