#ifndef SCDOC_UTF8_H
#define SCDOC_UTF8_H
#include <stddef.h>
#include <stdint.h>

#define UTF8_MAX_SIZE 4

#define UTF8_INVALID UINT32_MAX

/**
 * Returns the number of bytes required to encode the UTF-8 code point.
 */
size_t utf8_len(uint32_t ch);

/**
 * Grabs the next UTF-8 code point and returns its length.
 */
size_t utf8_decode(uint32_t *cp, const char *src);

/**
 * Encodes a code point as UTF-8 and returns its length.
 */
size_t utf8_encode(char *dst, uint32_t ch);

/**
 * Reads and returns the next code point from stdin.
 */
uint32_t utf8_get(void);

/**
 * Writes the code point to stdout.
 */
void utf8_put(uint32_t ch);

#endif
