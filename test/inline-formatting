#!/bin/sh
. test/lib.sh

begin 'Disallows nested formatting'
scdoc << EOF > /dev/null
test(8)

_hello *world*_
EOF
end 1

begin 'Ignores underscores in words'
scdoc << EOF | grep -Fqv 'fR'
test(8)

hello_world
EOF
end 0

begin 'Ignores underscores in underlined words'
scdoc << EOF | grep -Fqx '\fIhello_world\fR'
test(8)

_hello_world_
EOF
end 0

begin 'Ignores underscores in bolded words'
scdoc << EOF | grep -Fqx '\fBhello_world\fR'
test(8)

*hello_world*
EOF
end 0

begin 'Emits bold text'
scdoc << EOF | grep -Fqx 'hello \fBworld\fR'
test(8)

hello *world*
EOF
end 0

begin 'Emits underlined text'
scdoc << EOF | grep -Fqx 'hello \fIworld\fR'
test(8)

hello _world_
EOF
end 0

begin 'Handles escaped characters'
scdoc << EOF | grep -Fqx 'hello _world_'
test(8)

hello \_world\_
EOF
end 0
