printf '== %s\n' "$0"
trap 'echo' EXIT

begin() {
	printf '%-50s' "$1"
}

scdoc() {
	./scdoc 2>&1
}

end() {
	if [ $? -eq "$1" ]; then
		echo 'OK'
	else
		echo 'FAIL'
	fi
}
